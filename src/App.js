import React from 'react';
import {Route,Switch} from "react-router-dom";
import Home from "../src/Components/Home/Home";
import Auth from "../src/Components/Auth/Auth";

function App() {
  return (
    <div>
      <Switch>
       <Route exact path="/" component={Auth}/>
       <Route path="/home" component={Home}/>
      </Switch>
    </div>
  );
}

export default App;
