import React from "react";
import { withRouter } from "react-router-dom";

class Home extends React.Component {
  constructor(props) {
    super(props);

    const token = localStorage.getItem("token");

    if (!token) {
      return this.props.history.push("/");
    }
  }
  render() {
    return <div>Home Component</div>;
  }
}

export default withRouter(Home);
