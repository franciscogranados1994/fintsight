import React from "react";
import { withRouter } from "react-router-dom";
import { AuthMethod } from "../../Auth.Methods";
import { ReactComponent as Logo } from "../../Assets/images/logo.svg";
import "../Auth/Auth.scss";

class Auth extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      emailStyle: "neutro",
      emailComponent: true,
      emailValidation: true,
      emailEmpty: false,
      emailInvalid: false,
      password: "",
      passwordStyle: "neutro",
      passwordComponent: false,
      passwordValidation: true,
      passwordEmpty: false,
      passwordInvalid: false,
      recoveryCode: "",
      recoveryPassword: false,
      resetCode: false,
      newPassword: false,
    };

    this.emailRef = React.createRef();
    this.passwordRef = React.createRef();
  }

  async handleEmail(e) {
    const nodeEmail = this.emailRef.current.defaultValue;
    const value = e.target.value;
    const pattern = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
    const emailTest = pattern.test(e.target.value);

    if (!emailTest) {
      this.setState({
        emailStyle: "neutro",
        emailValidation: false,
      });
    } else {
      this.setState({
        emailValidation: true,
        emailStyle: "valid",
      });
    }

    if (nodeEmail.length > 0) {
      this.setState({
        emailEmpty: false,
      });
    }

    await this.setState({
      email: value,
    });
  }

  async handlePassword(e) {
    e.preventDefault();
    const nodePassword = this.passwordRef.current.defaultValue;
    const pattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
    const passwordTest = pattern.test(e.target.value);

    if (nodePassword.length > 0) {
      this.setState({
        passwordEmpty: false,
      });
    }

    if (!passwordTest) {
      this.setState({
        passwordStyle: "neutro",
        passwordInvalid: false,
        passwordValidation: false,
      });
    } else {
      this.setState({
        passwordStyle: "valid",
        passwordValidation: true,
      });
    }

    await this.setState({
      password: e.target.value,
    });
  }

  async handleEmailData(e) {
    e.preventDefault();
    //references
    const nodeEmail = this.emailRef.current.defaultValue;

    if (!this.state.emailValidation) {
      this.setState({
        emailStyle: "invalid",
      });
      return null;
    }
    if (nodeEmail.length === 0) {
      this.setState({
        emailStyle: "invalid",
        emailEmpty: true,
      });
      return null;
    }

    const result = await AuthMethod.loginEmail("a");

    //status is 200 and the user is not blocked by the system
    if (result.status === 200) {
      this.setState({
        emailComponent: false,
        passwordComponent: true,
      });
    }

    if (result.statusError) {
      this.setState({
        emailStyle: "invalid",
        emailInvalid: true,
      });
    }
  }

  async handlePasswordForm(e) {
    e.preventDefault();

    const nodeEMail = this.passwordRef.current.defaultValue;

    if (nodeEMail.length === 0) {
      this.setState({
        passwordStyle: "error",
        passwordEmpty: true,
      });
      console.log("NO puede ser 0");
      return;
    } else if (nodeEMail.length < 7) {
      console.log("Minimo 8 caracteres");
      return;
    }

    const result = await AuthMethod.loginPassword("a");

    if (result.status === 200) {
      return this.props.history.push("/home");
    } else {
      this.setState({
        passwordStyle: "invalid",
        passwordInvalid: true,
      });
    }
  }

  recoveryPassword() {
    this.setState({
      passwordComponent: false,
      recoveryPassword: true,
    });
  }

  redirectToBegging() {
    this.setState({
      emailComponent: true,
      recoveryPassword: false,
      resetCode: false,
    });
  }

  redirectToReset() {
    this.setState({
      resetCode: false,
      recoveryPassword: true,
    });
  }

  reStore() {
    this.setState({
      recoveryPassword: false,
      newPassword: true,
    });
  }

  enterResetCode() {
    this.setState({
      recoveryPassword: false,
      resetCode: true,
    });
    console.log("reset code");
  }

  render() {
    return (
      <div className="Auth">
        <div className="Auth_div-bg">
          <Logo />
        </div>

        {/*Email Component*/}

        {this.state.emailComponent ? (
          <div className="Auth_div-data">
            <div className="Auth_div-center">
              <h2 className="Auth_h2">¡Bienvenido!</h2>
              <h3 className="Auth_h3">Inicio de sesion</h3>
              <form
                onSubmit={(e) => {
                  this.handleEmailData(e);
                }}
                className="Auth_form"
              >
                <label className="Auth_label">
                  Ingresa el correo electronico
                </label>
                <input
                  ref={this.emailRef}
                  value={this.state.email}
                  onChange={(e) => {
                    this.handleEmail(e);
                  }}
                  name="email"
                  placeholder="Email"
                  className={`Auth_input-${this.state.emailStyle}`}
                />
                {/*Errors notification*/}
                {this.state.emailInvalid ? (
                  <span className="Auth_form-span">
                    Este correo no pertenece a ninguna cuenta. Por favor,
                    verificalo
                  </span>
                ) : null}

                {!this.state.emailValidation ? (
                  <span className="Auth_form-span">El correo es invalido</span>
                ) : null}

                {this.state.emailEmpty ? (
                  <span className="Auth_form-span">No debe estar vacio</span>
                ) : null}

                {/*  
                <span className="Auth_form-span">
                  Usuario Bloqueado. Comunicate con <a>Soporte Tecnico</a>
                </span> */}

                {/*Errors notification*/}

                <div className="Auth_div-button">
                  <button className={`Auth_button-${this.state.emailStyle}`}>
                    CONTINUAR
                  </button>
                </div>
              </form>
            </div>
            <div className="Auth_div-terms">
              <h4 className="Auth_h4-terms">
                Sujeto a la{" "}
                <a className="Auth_a" href="#">
                  Politica de Privacidad
                </a>{" "}
                y
                <a className="Auth_a" href="#">
                  Terminos y Condiciones
                </a>{" "}
                del Servicio. PROSA Derechos reservados 2020.
              </h4>
            </div>
          </div>
        ) : null}

        {/*Password Component*/}

        {this.state.passwordComponent ? (
          <div className="Auth_div-data slide">
            <div className="Auth_div-center">
              <h2 className="Auth_h2">Hola Helena</h2>
              <h3 className="Auth_h3">Ingresa tu contraseña</h3>
              <form
                onSubmit={(e) => {
                  this.handlePasswordForm(e);
                }}
                className="Auth_form"
              >
                <input
                  ref={this.passwordRef}
                  value={this.state.password}
                  onChange={(e) => {
                    this.handlePassword(e);
                  }}
                  name="password"
                  placeholder="Password"
                  className={`Auth_input-${this.state.passwordStyle}`}
                />

                {!this.state.passwordValidation ? (
                  <span className="Auth_form-span">
                    Debe contener al menos manyuscula, minuscula,numero y un
                    caracter especial
                  </span>
                ) : null}

                {this.state.passwordEmpty ? (
                  <span className="Auth_form-span">No debe estar vacio</span>
                ) : null}

                {this.state.passwordInvalid ? (
                  <span className="Auth_form-span">
                    La contraseña no es correcta
                  </span>
                ) : null}

                <div className="Auth_div-button-password">
                  <a
                    onClick={() => {
                      this.recoveryPassword();
                    }}
                    className="Auth_a"
                    href="#"
                  >
                    Olvide mi contraseña
                  </a>
                  <button className={`Auth_button-${this.state.passwordStyle}`}>
                    CONTINUAR
                  </button>
                </div>
              </form>
            </div>
            <div className="Auth_div-terms">
              <h4 className="Auth_h4-terms">
                Sujeto a la{" "}
                <a className="Auth_a" href="#">
                  Politica de Privacidad
                </a>{" "}
                y
                <a className="Auth_a" href="#">
                  Terminos y Condiciones
                </a>{" "}
                del Servicio. PROSA Derechos reservados 2020.
              </h4>
            </div>
          </div>
        ) : null}

        {/*Recovery Component*/}

        {this.state.recoveryPassword ? (
          <div className="Auth_div-data slide">
            <div className="Auth_div-center">
              <h2 className="Auth_h2">¡Hola, Elena!</h2>
              <h3 className="Auth_h3-reset">
                Te mandaremos un codigo de 4 letras para restablecer tu codigo
                de acceso al correo:
              </h3>
              <form
                onSubmit={(e) => {
                  this.handlePasswordForm(e);
                }}
                className="Auth_form"
              >
                <input
                  name="resetPassword"
                  placeholder={this.state.email}
                  className="Auth_input-recovery"
                />
                <div className="Auth_div-button-password">
                  <a className="Auth_a" href="#">
                    ¿Necesitas ayuda?
                  </a>
                  <button
                    onClick={() => {
                      this.enterResetCode();
                    }}
                    className={`Auth_button-${this.state.passwordStyle}`}
                  >
                    Enviar Codiigo
                  </button>
                </div>
              </form>
            </div>
            <div className="Auth_div-terms">
              <i
                onClick={() => this.redirectToBegging()}
                class="fas fa-chevron-circle-left  fa-2x logo"
              ></i>
              Regresar
            </div>
          </div>
        ) : null}

        {/*Code Recovery Component*/}

        {this.state.resetCode ? (
          <div className="Auth_div-data slide">
            <div className="Auth_div-center">
              <h2 className="Auth_h2">Hola Helena</h2>
              <h3 className="Auth_h3-reset">
                Se ha mandado correctamente el código a tu correo, ingresalo
                para restablecer tu contraseña
              </h3>
              <form className="Auth_form">
                <div className="Auth_div-inputs">
                  <input className="Auth_input-reset" />
                  <input className="Auth_input-reset" />
                  <input className="Auth_input-reset" />
                  <input className="Auth_input-reset" />
                </div>

                <div className="Auth_div-button-password">
                  <a className="Auth_a" href="#">
                    ¿Necesitas ayuda?
                  </a>
                  <button
                    onClick={() => {
                      this.reStore();
                    }}
                    className="Auth_button-reset"
                  >
                    RESTABLECER CONTRASEÑA
                  </button>
                </div>
              </form>
            </div>
            <div className="Auth_div-terms">
              <i
                onClick={() => {
                  this.redirectToReset();
                }}
                class="fas fa-chevron-circle-left fa-2x logo"
              ></i>
              Regresar
            </div>
          </div>
        ) : null}
        {this.state.newPassword ? (
          <div className="Auth_div-data slide">
            <div className="Auth_div-center">
              <h2 className="Auth_h2">¡Hola, Elena!</h2>
              <h3 className="Auth_h3-reset">
                Es momento de restablecer tu contraseña
              </h3>
              <form
                onSubmit={(e) => {
                  this.handlePasswordForm(e);
                }}
                className="Auth_form"
              >
                <input
                  name="resetPassword"
                  placeholder="Ingresa"
                  className="Auth_input-recovery"
                />
                <input
                  name="resetPassword"
                  placeholder="Confirma"
                  className="Auth_input-recovery"
                />
                <div className="Auth_div-button-password">
                  <button
                    onClick={() => {
                      this.enterResetCode();
                    }}
                    className={`Auth_button-${this.state.passwordStyle}`}
                  >
                    Restablecer
                  </button>
                </div>
              </form>
            </div>
            <div className="Auth_div-terms">
              <i
                onClick={() => this.redirectToBegging()}
                class="fas fa-chevron-circle-left  fa-2x logo"
              ></i>
              Regresar
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}

export default withRouter(Auth);
